**Opening the session:**

- background and motivation:
	- main role: CTO, between developer and maintainer, product management
	- estimate time using Forgejo per week: two hours
- private forgejo instance
	- not up to date
	- reason: some problem with helm chart / renovate bump workflow
	- visits forgejo helm repository to check the situation
	- needed a moment to find releases tab


**PR Reviews**

- reviews one created by renovate
- reads renovate information
- merges


**CI / Packages**

- uses forgejo actions, likes the compatibility to GH
- misses native kubernetes support
- no number next to package registry repo tab
- package registry setup: write workflow, create app token, set them as secret in actions
	- deploy keys ssh-only
	- note: consider per-repo API deploy keys


**Notifications**

- does not use in-app notifications
- only marks them as read from time to time
- prefers notifications in email client


**Other**

- SSO: only uses SSO (no local accounts), would like to hide native login to not confuse users
