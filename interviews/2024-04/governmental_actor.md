**Opening the session:**

- actor at governmental scale
- used forgejo for experimenting
- collecting / backing up repositories in their institution that are developed at different platforms
- only publication, no active development or public registration planned for now
- hope to grow into the direction of running a forgejo that is for active project development, too
- motivation for forgejo: independence of single vendor
- main role: manager and enduser


**Takeaways:**

- consider improving ways to feature projects to external users of Forgejo 
