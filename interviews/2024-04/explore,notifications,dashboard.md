**Opening the session:**

- background and motivation:
	- main role: developer
	- self-hosting Forgejo since 2022, used GitHub before
	- estimate time using Forgejo: several minutes every day, in recent days more (several hours)
	- idea about projects: personal projects
- spent time on frontend customizations for their own forgejo
	- not many issues with frontend modifications
	- update customizations with every release
	- kinds of customizations: header and footer
- ask: last thing they did using Forgejo: 
	- played with actions
	- error message on top of the page
	~~~
	JavaScript promise rejection: invalid language tag: "". Open browser console to see more details.
	~~~
	- notifications icon removed for their own instance


**Dashboard**

- do: open dashboard
	- check recent activity


**Notifications**

On Codeberg.

- merged pull requests and closed issues
- not sure if they want to receive them
- the notifications are relevant
- usually already read via email
- click notifications
- pull request
- files changed
- browses repo
- marks all notifications on Codeberg as read, already seen via email
- user misses a button to mark single notifications quickly
	- reviewer points out there is such a **button on hover**


**Other**

- user surfing around
- explore view
- no special attention
- visits Clima (app with icon)
- user likes the way GitHub does filtering
	- uses github global search to find code snippets
	- language options in sidebar
	- user would use sidebar buttons over manually modifying the search query
	- using this for a few months only
- git clone by ssh is broken on their instance, it worked in the past
- they say they might have closed the SSH port

