**Opening the session:**

- main role: developer
	- Gitea user (for more than 1 year), waiting for NixOS Forgejo version
	- Gitea instead of GitLab because of arm64
	- upgraded to latest version
	- development as paid job (mostly on GitHub / GitLab)
- their mirror setup:
	- pushing through local instance -> pushing to remotes from there (push mirror)
	- "Sync when commits are pushed" very important push mirror setting
	- sometimes triggering some push mirrors manually (because of failed DNS lookups)


**Dashboard**

- login via OAuth from Codeberg
- daily action:
	- look at dashboard first to check if last pushes are reflected (check for missing commits)
	- network issues between local machine and Forgejo, so commits are often missing
	- checking that mirrors are working by visiting each repo's settings and looking for errors in the push mirror section
- note: **errors** that require a user's attention are a good fit for the **dashboard**


**Organization**

- do: create a new organization
	- uses "+" button for organization on the dashboard sidebar
	- new repo from navbar
- created repo in user scope first, not sure if this was a communication mistake
- by default, issues are disabled on new repos (their per-instance)
- confused about how to enable the issues tab
- add users
	- org settings
	- org teams (no button)
	- personal settings
	- admin settings
	- didn't find it, asked me for help


**Admin Settings**

- user needs to create users to complete the assignment task above
- admin settings: not obvious that **user accounts are under "identity and access"**
- **mass creation of users** is hard, because you always need to navigate back o the summary to create a new one


**Notifications**

- uses another public Gitea instance (also upgraded to the latest version)
- no unread, but two pinned
- notification volume seems right
- irrelevant notifications: depends … follows all activity of one repo to be warned of breaking changes
- gets all notifications via email
	- filters notifications by instance
	- reads all notifications in-app *again* to ensure they missed nothing
- notifications to other platforms (Discord in this case) would be great (direct message or group)
	- wants to receive notifications on pushes
	- already possible with admin -> system webhooks -> ...


**Navbar tabs**

- never used these before
- user seems generally confused about their appearance
- user recently created an issue
- checks milestones on GitHub to see where to send a PR
- shows github, they also have a global issue tab to the right
	- suggestion of user: move to the right, so it is clear that it is "relevant to you"


**Organization Part Two**

- ask them to add another contributor to the organization they created before and see if they can complete the action quickly[^1]
- remembers how to do it, but a little slow


**Other**

- wants to have better documentation
- Gitea docs about rootless podman not up to date
- uses go to file, relevant entries but also a lot of noise
- native packages
- someone told them that Forgejo improved a lot over Gitea


**Takeaways**

- mass-creation of users is a feature many people might want to have, also relevant e.g. to coding classes etc
- the admin sidebar tab names are super confusing
