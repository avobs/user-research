**Accessibility**

- considering to host Forgejo for their company
- jaws screen reader for windows
- contributes to accessibility on a technical level
- ask: why not more?
	- is a backend developer
	- can do improvements for templates, but not for JS
	- doing frontend development does not make sense for a blind person
	- struggled with frontend framework in Forgejo to fix things like simple buttons


**Pull Requests**

- focus for the session is determined to be pull request review
- do: navigate forgejo
- note: **blind users like good urls**, often navigate by direct url access
- a lot of redundant links in pull request list, but not a big problem
- **unlabelled button** in file view (copy name)
- pr comment works
- previous / next button
	- unclear what they mean
	- context conveyed by visual elements only
	- suggestion by reviewer: "Next comment"
	- confirmed
	- suggestion by user: disable buttons if there is no next/last comment (only one comment in our case)
- **emoji + dots menu** not accessible for comments
- **review submission** area is at the end of the page in taborder
	- suggestion: make it modal dialog
- reads "Review 1"
	- idea: add label to clarify meaning
	- e.g. "Finish review, 1 pending"


**Other**

- ask: should search bars add the context, like "Search repos" instead of "Search"
	- user confirms
	- further suggestion: put filters before search button / submit button (so that submit button is always at the end of the form)
- confirm: splitting link to repo to user and repo part is good
- suggestion: explore view, **label "language: X"** 
- ask: is there a best practice to indicate button roles? (Note for context: See https://codeberg.org/forgejo/forgejo/issues/3142)
	- thinks about it
	- no best practice
- general observation: "Forgejo is over-obsessed with menus""
- ask: best practice for handling focus for SPA or javascript-based interactions
	- suggestion: aria-live regions, but not a lot (maybe even aria-live-polite)
- general suggestion: add more settings to fine-tune the behaviour for the user


**Takeaways**

In all cases, please read the full log. There are many valuable insights.
The most important keys for me:

- overriding button context with labels makes sense when the position of the button also conveys context
	- example: Previous / Next buttons are in the comment box and convey their meaning to a sighted user,
	  labelling them "Previous comment" helps to add more context
- Forgejo admin panel not yet accessible (kind of a don't care case) -> this user demonstrates relevance
