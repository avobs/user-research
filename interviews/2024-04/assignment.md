**Opening the session:**

- background and motivation:
	- main role: IT-admin
	- likes self-hosting
- idea about projects:
	- personal projects on Forgejo
	- projects at paid job are on different platforms
	- some stale projects on GitHub could be moved if they ever get active again
- specific features used
	- markdown documentation
	- Actions for building containers
- also uses Gitea with a colleague
- considered contributing to Forgejo


**General Feedback**

- admin checker in forgejo not always updates
- only for major releases, not patch versions
- reviewer cannot help with this problem


**Contributing to a project**

- considered to contribute to Forgejo
- but no idea what to do
- do: find an issue that interests you via the label part
- user selects accessibility label
- user visits one issue about dark theme icon contrast
- is interesting to them
- user clones repository and explores the codebase
- cancelled after some minutes in favour of assignee user testing
- asked to indicate their own commitment to the issue
- **user assigned themself** (he got permission from reviewer)
- do: explore issues and find another one where someone indicated interest
- technical difficulties and confusion about the task lead to no good result, my notes are incomplete
- in the end, the user managed to assign the user who indicated interest


**Takeaways**

- tt might be interesting to let users assign themself to issues, even when they don't yet have the permission to do this
