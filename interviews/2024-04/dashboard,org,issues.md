**Opening the session:**

- Forgejo self-hoster
- background and motivation:
	- main role: developer, pull request review
	- estimate time using Forgejo: once a week for a few hours


**Dashboard**

- do: perform first daily action
	- checks for updates (someone pushed changes)
	- from dashboard, visits recent commits
	- checks updates in repo
	- back to dashboard, scans weblate commits
	- visits organization from dashboard sidebar, visits repo
- ask: regular interactions with it?
- ask: expectations, missing items
	- no way to sort
	- reviewer hint: context switcher in top left
- ask: why visit repos one by one
	- take a look at commit log per repo


**Organization**

- do: create new organization
	- how do they do this? ([ ] navbar plus [x] dashboard sidebar plus)
	- which fields? confusion?
- do: create new repo for discussions with only issue tracker enabled
	- how do they create the repo? dashboard sidebar
	- notes on new repo dialog: pretty straightforward
	- notes on repo settings: a lot of scrolling up and down
- do: add two contributors to the org
	- explain: direct adding to repo vs org
	- do: add them via team
	- visits:
		- teams
		- members
		- teams
		- owners team
		- finds add user button there
- demonstrates GitHub workflow
	- inviting member is easy
	- after inviting, you get asked in which team to add the person


**Notifications**

- note: number of unread: 0
- ask: notifications via email?
	- yes everything via email
	- can check on any device
	- ignores in-app notifications


**Navbar tabs**

- ask: did they use the tabs before
	- no, never used before
	- could help optimize work
	- user has "archive" org and ingores content in there
	- would be good to exclude the archive org from the view


**Takeaways**

Many places in Forgejo allow to see all or filter for one. It would be great to have more advanced filter options, especially allowing to exclude something.

