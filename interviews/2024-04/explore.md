**Opening the session:**

- background and motivation:
	- main role: contributor, walking around and sending PRs
	- Codeberg user
	- estimate time using Forgejo: ~ 5 hours per week, most projects are on GitHub
	- Codeberg mostly for small projects
- ask: how do they discover projects?
	- browsing mastodon
	- word by mouth
	- most of them are on GitHub
- ask: content on Codeberg?
	- knows pipepipe
- do: explore view, interesting matches?
	- picked agreoecology map
	- likes maps, searches "map"
	- not many results


**Notifications**

- note: 1 unread, but rather old
- ask: notifications via email? yes


**Other**

- ask: miss sth. on Codeberg compared to other platforms?
	- repo pinning on GitHub (example with orgs)

**Takeaways**

- word of mouth is crucial to promote projects that are not on GH and provide more visibility to the platforms itself
	- I'll give my efforts higher priority with Codeberg, might be a good idea for Forgejo, too (showing content hosted on small Forgejo intances)
