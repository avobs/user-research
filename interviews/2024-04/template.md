General questions to keep in mind during the session:

- Are there generic obstacles when navigating the page?
- Are there special moments of delay, e.g. caused by slow page loads, confusion or otherwise unexpected behaviour?
- When navigating the page, are there **frequent / repeated** actions that are **placed far apart**
  (e.g. visually or tab order on keyboard navigation?)


**Further Ideas**

- self-hosters? -> Moderation
- working with issue labels
- pr reviews


**Opening the session:**

- welcome, thanks, state the goal
- no audio/video/screen recording, but anonymized notes
- background and motivation:
	- main role: developer | maintainer | manager | translator | enduser | designer | something else?
	- estimate time using Forgejo: 
	- idea about projects:
- screenshare:
	- whole screen (for the visibility of context menues)
	- recommend disabling notifications and entering fullscreen mode (prevent information leak)
- ask: last thing they did using Forgejo: 
- encourage "thinking aloud"
	- and sometimes remind them about it, for example by asking "What are you thinking right now?"
- ask: typical **first action of the day**
	- e.g. checking notifications, visit dashboard
	- let them perform their routine if it matches one of the tasks below or doesn't take significant time


**Dashboard**

- do: perform first daily action
	- ask: improvements to dashboard that could help here
- do: open dashboard
- ask: regular interactions with it?
- ask: expectations, missing items
- does the user revisit the dashboard during further tasks, and why?


**Organization Creation**

- do: create new organization
	- how do they do this? ([ ] navbar plus [ ] dashboard sidebar plus)
	- which fields? confusion?
- do: create new repo for discussions with only issue tracker enabled
	- how do they create the repo?
	- notes on new repo dialog:
	- notes on repo settings:
- do: add two contributors to the org
	- explain: direct adding to repo vs org
	- do: add them via team


**Notifications**

- note: number of unread:  | number of pinned: | number of read: 
- ask: notifications via email?
	- ask: specific advantages with email?
	- ask: problems / challenges?
- ask: miss important news and why?
- ask: get noise / less relevant notifications?
- ask the user about estimate notification frequency
- if multiple unread: ask how they priorize
- if they open one: ask: relevant? high priority? why did they receive this one?
- ask: any challenges? other thoughts?


**Navbar tabs**

- if last action: do: continue with it
- ask: did they use the tabs before
- ask: challenges, obstacles, comments on the tabs?


**Organization Part Two**

- do: add another contributor to org
- do: show their org membership on their profile
	- hint: go to org user list, toggle there
