**Opening the session:**

- Forgejo user and contributor to the Forgejo ecosystem
- both self-hoster and public instance user
- background and motivation:
	- company uses forgejo
	- main role: poweruser (dev + management + maintainer)
- ask: last thing they did using Forgejo: made a PR in a single-user project with CI
- ask: typical **first action of the day**
	- checks for **spam users** on own instance
	- goes to admin settings
	- **unfold identity**, unfold user list, sort by newest
	- visits user, clicks public activity
- uses email notification for new users
	- has a script that checks new users
	- checks suspicious users once a month
	- e.g. checking notifications, visit dashboard
	- let them perform their routine if it matches one of the tasks below or doesn't take significant time


**Notifications**

- checks notifications on Codeberg
- not visible which kind of activity (mention, assign etc) caused the notification
- navigates to issues tab
- navigate to repo
- ask: why watching this specific repository?
- because of auto-subscribe with permissions


**Dashboard**

- user asks for a less noisy dashboard
- would like to view all organizations at once
- recommends featured projects


**Explore**

- navigates explore view
- tries search query "language:go"
- seconds ideas about a more "featured" way for exploration,
- and a unified search box (instead of preselecting what you want to search)


**Notifications**

- note: number of unread: 2


**Takeaways**:

- admin: sidebar navigation (unfolding the sections in the sidebar) is not convenient
- admin: user view and details should be improved drastically with spam defense in mind
- notifications: should indicate what caused them (e.g. mentions, assigned etc)
- dashboard: should allow to see activity of all projects at once
