**Opening the session:**

- background and motivation:
	- Codeberg user
	- main role: user or small script developer
	- downloading software, exploring new projects, contacting the maintainers
	- not often as signed-in user
	- estimate time using Forgejo: not regularly 


**Explore**

- not signed in
- landing page
- not a lot of things
- clicks explore
- scrolls to bottom
- clicks last page
- didn't want to do this, returns
- next page
- knows Gadgetbridge, clicks
- **scrolls immediately to README**
- notices repo topics for the first time
	- clicks android-application topic
	- not many results
	- goes back, clicks android instead
	- more results
- finds localsend
- finds phonograph plus
- appreciates screenshots in repo
- finds transistor app
- repos with icon are much more attracting
- ask: already contributed to projects?
	- translations via PR on GitHub
- do: join translations here
	- clicks weblate link in readme
	- browses weblate
	- user is surprised they can actually work with the software without an account (expected to be forced to sign up first)
- back to forgejo
- often browses releases, issues, last commit
- ask: how get app?
	- usually check readme for fdroid link
	- ask: click readme badge?
	- no, often discovered via fdroid, and anyway easier to search for the app on fdroid rather than opening the link
- ask: would a central place for external sources help?
	- yes, **readmes are not consistent**, takes time to see if it is in fdroid, ...
	- maybe on releases instead of direct apk download
- ask: you said you check the latest commits, also the code?
	- no , don't check the code
- ask: reaction to **summary page** of a project instead of code (e.g. latest activity, project links, README below, no code tree (would be an extra tab))
	- yes, would give important information on a glance
- sometimes, with a big tree, the README is way below
- signs in
- stars repo
	- checks dashboard, star does not show up
	- finds it in menu
	- explains that GitHub has folders for favorites, user appreciates this
- **codeberg dark theme**: star buttons somehow appear boring / "disabled" (colour difference not big to disabled)
- user says he would appreciate **visual feedback**, e.g. a coloured star icon


**Notifications**

- note: number of unread: 1, already read via email


**Takeaways**

- repo topic proliferation makes discovering projects more difficult, we might want to think about how we can encourage consistent labelling (e.g. "android" vs "android-application")
- improve Forgejo for users who explore and discover projects
	- ideas: repo summary which shows activity, README, download sources, code remains in code tab
