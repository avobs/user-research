**Opening the session:**

- background and motivation:
	- self-hosting Gitea since two years, moved to Forgejo
	- also collaborating on Codeberg
	- main role: mostly developer, sometimes code review (5-10%)
	- GitHub on day job
	- estimate time using Forgejo: Codeberg on weekends (2-3h)
	- uses local text editor on local instance for text editing (1h/week)
- ask: last thing they did using Forgejo: repository maintenance (backup of archived GitHub projects)
- ask: issues when selfhosting?
	- painful migration
	- in part due to bare metal to container migration with different file structure
- token for git mirroring failed for some repos; error description unclear ("could not connect to upstream" or similar), after restarting mirror it worked
- ask: typical **first action of the day**: open dashboard, see next


**Dashboard**

- do: perform first daily action
	- open dashboard
	- looks what happened in the last days / weeks
	- checks comments on issues


**Notifications**

- does not use notifications immediately, but after they updated themself via other means
- ask: would it help to group notifications per repo?
	- yes, nice idea


**PR review**

- was pending anyway, navigates to the files view of the PR
- uses "viewed" checkbox per file
- user misses viewing file changes per commit
- interviewer points to filter-per-commit button
- views one commit
- ask: is marking files as viewed relevant in per-commit view?
- confirm: not necessary
- collapsing and commenting is important in per-commit view, is present
- user expects to **select multiple lines for the comment**
- **"changed since your last review"** marker
	- might be cool if it shows a diff / split screen
	- maybe a button to show the changes since last review
	- it can be some time (in this case: some months) since the last review
- user wonders about the **"vendored" label** for gitignore file
	- tries to obtain more information via hover, but there is no information
- user expects that **review replies also get pending**, not only new comments
	- note: inconsistency between new and existing comments
- user loves monospace font in text editor
- adding comment to line is a little **slow** (1 - 1.5 seconds)
- user reported some buggy state in the past, but couldn't provide further details
- ask: used file tree view?
	- no, not used to it, also a new feature on other platforms
	- clicks file
	- already viewed file is expanded (probably was collapsed before)
	- note to self: check if **collapse behaviour** is working fine


**Takeaways**

- Review: many improvement ideas for PR review, see emphasis
- Notifications/Dashboard: Group per repo
