# User Interviews Take One

The first round of Forgejo user interviews was conducted.
Over a course of one week (2024-04-08 to 2024-04-14),
a total of 18 interviews have been made with a diversity of Forgejo users.

*(Note: There are only 17 text files about the interviews available.
One interview was an interesting conversation,
but the discussion out of scope for this user research.
The notes are not included here)*


## Methodology

- Nextcloud Calendar for booking appointments
- several reserved timeslots, initially scheduled for 30 minutes per interview, but at least 45 is better
- announcement on Codeberg's Fediverse profile: https://social.anoxinon.de/@Codeberg/112232207025500056
- announcement in "Forgejo Chat" matrix channel: https://matrix.to/#/%23forgejo-chat%3Amatrix.org/%24wobvPZyuGjTcQolvGzJQMuuboZpDCNDTn6u8780oYHE?via=matrix.tu-berlin.de&via=turbo.ooo&via=matrix.org&via=catgirl.cloud
- I manually sent the participants invite links to a BigBlueButton conference via screenshare
- nothing was recorded, but I copied notes into textfiles before each meeting
- people used Codeberg or their Forgejo instance of choice to complete tasks
- to test a specific new feature (https://codeberg.org/forgejo/forgejo/pulls/2283), the patch was deployed to https://staging.codeberg.org
	- users could log in with their Codeberg useraccount and work with a snapshot of production data
	- it allowed to test with real world data, but without the fear of destroying
	- people were added to the Forgejo org to allow them assigning users
	- there is a high potential in testing new features and designs this way, also moderation and similar


## Diversity

The interviews were conducted with a diversity of target groups.
We didn't only gain insights about developers and maintainers,
the primary user group of Forgejo,
but also about users who only want to discover and interact with projects,
and who are not interested in participating in software development on a technical level.

The interviews included one blind user,
which helped gain valuable insights into the accessibility aspect of Forgejo.
We got confirmation on certain aspects we addressed in the past,
and concluded a list of improvements for the near future of Forgejo.

Additionally, one user with a mild form of autism helped us get feedback on more accessibility properties
in addition to being screenreader accessible.

The participation consisted not only of Europeans,
but included people from Northern America and Asia,
although much less than guests from Europe.
Users from Africa and Southern America were probably underrepresented in the research at this point.

Although we did not directly collect more details from the guests,
I subjectively believe that people who identify differently than cis-male were also underrepresented in our research.
During the week, a Mastodon post by Codeberg called for people from this group specifically,
but there was no further interview scheduled in reaction to this.

For Forgejo to become a truly inclusive software forge,
we might want to consider encouraging users from underrepresented groups to participate in the interviews,
so that we can identify challenges that affect these users in particular.


## General Impressions

Some things I want to share, because I think they generally help in making Forgejo a better product.

**Settings are often confusing.**
They were usually designed with developers in mind who need this specific feature.
However, even developers who don't can get confused by the amount of settings.
One example: The permission system for org teams lists "Actions" as one group.
Compared to the others, it lacks a description of what this is about.
To people who have used "Actions" for CI on GitHub,
this is probably self-explanatory.
For users who haven't used CI, or never heard of this specific CI,
this setting is confusing, because the term "Actions" itself could mean many different things.

**Descriptions help.**
In many cases, there are helper strings below settings and checkboxes.
These save the need to visit external documentation
and help users who are not familiar with the menus.
I recommend to invest effort in improving the descriptions and adding more of them where they lack.
We might save the effort and don't redundantly describe them in our documentation,
unless there is more information to be added.

**Our settings are not consistent.**
There is a lot of scrolling, especially in the repository settings.
We might want to create guidelines for settings and revisit existing ones
(e.g. one sidebar tab per section).
Certain settings sections should probably be recreated and optimised for an easier overview,
especially the repository settings and the create repo dialog.

**Tab labels should be more intuitive.**
There are places where we seem to find a tab label that is technically the best description,
but is unintuitive for a user and prevents them from quickly navigating through the app.
Example: An admin who wants to add a new user might expect "Users" or "Accounts",
but needs to unfold "Identity and Access" which does not contain the keywords they are looking for.

**Search syntax.**
Users try to intuitively use search syntax in many places.
None of this is supported.
Using a standard search syntax could also improve consistency across search and filter options.

**Filters: Exclude over Include.**
Forgejo has many filters, for example in the issues tab or for the dashboard context.
They often allow seeing only one unit, or sometimes all.
I got the impression that many users would like to see everything at once (e.g. on the dashboard),
and that the case to exclude one or two things (e.g. orgs) is much better than visiting them one after another.

**Persistent filters.**
Some people seem to have loved filter options to persist,
e.g. on the dashboard sidebar.
Maybe we can implement a generic way for users to save filters as the default for this page,
or save named profiles.


## Dashboard

Users often seem to visit projects one by one instead of trying to get a chronological summary.
Any redesign should take this into account.

Users want to see the status of their projects (e.g. errors, failed CI pipelines etc).


## Precise issues

I'll try to create individual issues where I can derive a concrete actionable improvement.
I am not going to track them here, though.


## Other notes

I think we should try to narrow the scope for the future,
e.g. do research for code review only.
It will help being more focused,
and get more feedback on precise improvements.


~ fnetX
